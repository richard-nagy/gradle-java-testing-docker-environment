FROM ubuntu:latest

ENV GRADLE_VERSION="7.3.3"

# Update package manager and packages
RUN apt update -y && \
    apt upgrade -y

# Install necessary packages
RUN apt install -y wget && \
    apt install -y unzip

# Install JDK
RUN apt install -y openjdk-11-jdk && \
    java -version

# Install Gradle (into /opt/gradle)
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -P /tmp/gradle && \
    unzip -d /opt/gradle /tmp/gradle/gradle-${GRADLE_VERSION}-bin.zip && \
    ln -s /opt/gradle/gradle-${GRADLE_VERSION} /opt/gradle/latest && \
    export GRADLE_HOME=/opt/gradle/latest && \
    export PATH=${GRADLE_HOME}/bin:${PATH}

# Add Gradle to path
ENV GRADLE_HOME=/opt/gradle/latest
ENV PATH=${GRADLE_HOME}/bin:${PATH}

# Move to source directory
WORKDIR /usr/project
RUN gradle -v

# Endlessly running command to keep the container running
ENTRYPOINT ["tail", "-f", "/dev/null"]

# operation was attempted on something that's not a socket